"""
Функция dict_to_list.

Принимает 1 аргумент: словарь.

Возвращает (список ключей,
            список значений,
            количество уникальных элементов в списке ключей,
            количество уникальных элементов в списке значений).

Если вместо словаря передано что-то другое, то возвращать строку 'Must be dict!'.
"""