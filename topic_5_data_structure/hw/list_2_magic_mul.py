def magic_mul(my_list):
# """
# Функция magic_mul.
#
# Принимает 1 аргумент: список my_list.
#
# Возвращает список, который состоит из
#     [первого элемента my_list]
#     + [три раза повторенных списков my_list]
#     + [последнего элемента my_list].
#
# Пример:
#     входной список [1,  ‘aa’, 99]
#     результат [1, 1,  ‘aa’, 99, 1,  ‘aa’, 99, 1,  ‘aa’, 99, 99].
#
# Если вместо списка передано что-то другое, то возвращать строку 'Must be list!'.
# Если список пуст, то возвращать строку 'Empty list!'.
# """
    if type(my_list) != list:
        return 'Must be list!'
    elif len(my_list) == 0:
        return 'Empty list!'

    # return my_list[:1] + my_list*3 + my_list[-1]

    res_list = []
    res_list.extend(my_list[:1])
    res_list.extend(my_list[::1]*3)
    res_list.append(my_list[-1])
    return res_list

if __name__ == '__main__':
    print(magic_mul([1, 2, 'def']))

