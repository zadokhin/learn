def get_info_for_3_set(left: set, mid: set, right: set):
    """
    Функция get_info_for_3_set.

    Принимает 3 аргумента: множества my_set_left, my_set_mid и my_set_right.

    Возвращает dict с информацией:
        {
        'left == mid == right': True/False,
        'left == mid': True/False,
        'left == right': True/False,
        'mid == right': True/False,

        'left & mid': set(...),     # intersection
        'left & right': set(...),   # intersection
        'mid & right': set(...),    # intersection

        'left <= mid': True/False,     # issubset
        'mid <= left': True/False,     # issubset
        'left <= right': True/False,   # issubset
        'right <= left': True/False,   # issubset
        'mid <= right': True/False,    # issubset
        'right <= mid': True/False     # issubset
        }

        Если вместо множеств передано что-то другое, то возвращать строку 'Must be set!'.
    """

    if type(left) != set or type(mid) != set or type(right) != set:
        return 'Must be set!'

    info_dict = {
        'left == mid == right': left == mid == right,
        'left == mid': left == mid,
        'left == right': left == right,
        'mid == right': mid == right,

        'left & mid': left.intersection(mid),  # intersection
        'left & right': left.intersection(right),  # intersection
        'mid & right': mid.intersection(right),  # intersection

        'left <= mid': left.issubset(mid),  # issubset
        'mid <= left': mid.issubset(left),  # issubset
        'left <= right': left.issubset(right),  # issubset
        'right <= left': right.issubset(left),  # issubset
        'mid <= right': mid.issubset(right),  # issubset
        'right <= mid': right.issubset(mid)  # issubset
    }

    return info_dict


if __name__ == '__main__':
    left = {1, 2, 3}
    mid = {1}
    right = {4, 5}
    print(get_info_for_3_set(left, mid, right))
