from itertools import zip_longest

def zip_names(names, family):
# """
# Функция zip_names.
#
# Принимает 2 аргумента: список с именами и множество с фамилиями.
#
# Возвращает список с парами значений из каждого аргумента.
#
# Если вместо list передано что-то другое, то возвращать строку 'First arg must be list!'.
# Если вместо set передано что-то другое, то возвращать строку 'Second arg must be set!'.
#
# Если list пуст, то возвращать строку 'Empty list!'.
# Если set пуст, то возвращать строку 'Empty set!'.
#
# Если list и set различного размера, обрезаем до минимального (стандартный zip).
# """
    if type(names) != list:
        return 'First arg must be list!'
    if type(family) != set:
        return 'Second arg must be set!'
    if len(names) == 0:
        return 'Empty list!'
    if len(family) == 0:
        return 'Empty set!'
    result = []
    result = list(zip(names, family))
    return result

if __name__ == '__main__':
    print(zip_names([1, 2, 3], {'3','4','5'}))
