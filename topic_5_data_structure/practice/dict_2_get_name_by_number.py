def get_name_by_number(name_num, num):
    """
    Функция get_name_by_number.

    Принимает 2 аргумента:
        словарь содержащий {имя: [телефон1, телефон2, ...], ...},
        номер телефона (строка) для поиска в словаре.

    Возвращает имя владельцев телефона (список), если такой номер есть в словаре, если нет, то 'No name'.

    Если вместо словаря передано что-то другое, то возвращать строку 'Dictionary must be dict!'.
    Если словарь пустой, то возвращать строку 'Dictionary is empty!'.

    Если вместо строки для поиска передано что-то другое, то возвращать строку 'Phone number must be str!'.
    Если строка для поиска пустая, то возвращать строку 'Phone number is empty!'.
    """

    if type(name_num) != dict:
        return 'Dictionary must be dict!'
    elif len(name_num) == 0:
        return 'Dictionary is empty!'

    if type(num) != str:
        return 'Phone number must be str!'
    elif len(num) == 0:
        return 'Phone number is empty!'

    result_names = []

    for name, pnums in name_num.items():
       if num in pnums:
            result_names.append(name)

    return result_names if len(result_names) > 0 else 'No name'


if __name__ == '__main__':
    my_dict = {'pete': ['777-44'],
               'ira': ['1234-555'],
               'anton': ['1234-555', '777-44']}

    my_result1 = get_name_by_number(my_dict, '1234-555')
    my_result2 = get_name_by_number(my_dict, '777-44')
    print(my_result1)
    print(my_result2)
