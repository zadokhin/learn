def count_num(my_list, num):
    """
    Функция count_num.

    Принимает 2 аргумента: список числами my_list и число num.

    Возвращает количество num в списке my_list.

    Если вместо списка передано что-то другое, то возвращать строку 'First arg must be list!'.
    Если вместо числа передано что-то другое, то возвращать строку 'Second arg must be int!'.
    Если список пуст, то возвращать строку 'Empty list!'.
    """
    error_string = ''

    if type(my_list) != list:
        # return 'First arg must be list!'
        error_string += 'First arg must be list!\n'
    elif len(my_list) == 0:
        # return 'Empty list!'
        error_string += 'Empty list!\n'

    if type(num) != int:
        # return 'Second arg must be int!'
        error_string += 'Second arg must be int!\n'

    if len(error_string):
        return error_string

    return my_list.count(num)


if __name__ == '__main__':
    print(count_num(None, 1))
    print(count_num([1], None))
    print(count_num([], 1))
    print(count_num([1, 'a'], 1))
    print(count_num([1, 1, 1], 1))
