def check_substr (str1, str2):
# """
# Функция check_substr.
#
# Принимает две строки.
# Если меньшая по длине строка содержится в большей, то возвращает True,
# иначе False.
# Если строки равны, то False.
# Если одна из строк пустая, то True.
# """
    len_diff = len(str1) - len(str2)
    if len_diff == 0:
        return (False)
    elif len(str1)== 0:
        return (True)
    elif len(str2)== 0:
        return (True)
    elif len_diff > 0:
        if str1.count(str2)>0:
            return (True)
        else:
            return (False)
    elif len_diff < 0:
        if str2.count(str1)>0:
            return (True)
        else:
            return (False)

if __name__ == '__main__':
    check_substr ('', '')
