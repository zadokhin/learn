def check_sum (num1, num2, num3):
# """
# Функция check_sum.
#
# Принимает 3 числа.
# Вернуть True, если можно взять какие-то два из них и в сумме получить третье, иначе False
# """
    if num3 == num1 + num2:
        return (True)
    elif num1 == num2 + num3:
        return (True)
    elif num2 == num1 + num3:
        return (True)
    else:
        return (False)

if __name__ == '__main__':
    check_sum (1,2,8)
