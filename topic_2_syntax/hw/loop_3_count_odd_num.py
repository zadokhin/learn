def count_odd_num (n):
# """
# Функция count_odd_num.
#
# Принимает натуральное число (целое число > 0).
# Верните количество нечетных цифр в этом числе.
# Если число равно 0, то вернуть "Must be > 0!".
# Если число не целое (не int, а другой тип данных), то вернуть "Must be int!".
# """
    ans = 0
    if not isinstance(n, int):
        return ("Must be int!")
    elif isinstance(n, bool):
        return ("Must be int!")
    elif int(n) <= 0:
        return ("Must be > 0!")
    else:
        for i in str(n):
            if int(i) % 2:
                ans += 1
        return ans

if __name__ == '__main__':
    count_odd_num (1234)