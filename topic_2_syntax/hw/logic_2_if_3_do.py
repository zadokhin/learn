def if_3_do (number):
# """
# Функция if_3_do.
#
# Принимает число.
# Если оно больше 3, то увеличить число на 10, иначе уменьшить на 10.
# Вернуть результат.
# """
    if number > 3:
        return (number+10)
    else:
        return (number-10)

if __name__ == '__main__':
    if_3_do (5)
