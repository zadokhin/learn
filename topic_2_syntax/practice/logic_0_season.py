def season(num_month):
    """
    Функция season.

    Принимает 1 аргумент — номер месяца (от 1 до 12).
    Возвращает время года, которому этот месяц принадлежит
    (Winter, Spring, Summer или Fall).
    """
    if num_month == 12 or num_month == 1 or num_month == 2:
        return 'Winter'
    elif num_month in (3, 4, 5):
        return 'Spring'
    elif num_month in (6, 7, 8):
        return 'Summer'
    elif num_month in (9, 10, 11):
        return 'Fall'


def main():
    print(season(3))
    print(season(6))


if __name__ == '__main__':
    main()
