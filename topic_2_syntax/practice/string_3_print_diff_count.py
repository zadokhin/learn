def print_diff_count(arg1, arg2):
    """
    Функция print_diff_count.

    Принимает две строки.

    Вывести большую по длине строку столько раз,
    насколько символов отличаются строки.

    Если строки одинаковой длины, то вывести строку "Equal!".
    """

    # len_1 = len(arg1)
    # len_2 = len(arg2)
    # if len_1 > len_2:
    #     print(arg1 * (len_1 - len_2))
    # elif len_1 < len_2:
    #     print(arg2 * (len_2 - len_1))
    # else:
    #     print('Equal!')

    len_diff = len(arg1) - len(arg2)
    if len_diff > 0:
        print(arg1 * len_diff)
    elif len_diff < 0:
        print(arg2 * -len_diff)
    else:
        print('Equal!')


if __name__ == '__main__':
    print_diff_count('123', '123456')
