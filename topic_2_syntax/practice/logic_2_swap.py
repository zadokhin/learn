"""
Функция swap.

Принимает 2 аргумента (first_param, second_param) с некоторыми значениями.
Поменять местами значения этих переменных и вывести на экран таким образом “first_param = 3 | second_param = True”.
"""