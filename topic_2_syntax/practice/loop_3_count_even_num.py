"""
Функция count_even_num.

Принимает натуральное число (целое число > 0).
Верните количество четных цифр в этом числе.
Если число равно 0, то вернуть "Must be > 0!".
Если число не целое (не int, а другой тип данных), то вернуть "Must be int!".
"""